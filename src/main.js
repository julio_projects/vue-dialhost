import '../theme/index.css'
import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/pt-br'
import DataTables from 'vue-data-tables'
import VueRouter from 'vue-router'
import axios from 'axios';
const moment = require('moment')
require('moment/locale/pt-br')

Vue.use(require('vue-moment'), {
    moment
});

Vue.use(ElementUI, { locale })
Vue.use(DataTables)
Vue.use(VueRouter)

// components
import usuario from './components/usuario.vue'
import usuarioEdit from './components/usuario-edit.vue'

const axiosConfig = {
    baseURL: 'http://localhost:8000/',
    timeout: 30000,
};
Vue.prototype.$axios = axios.create(axiosConfig);

const routes = [
  {
    path: '/',
    name: 'usuario',
    component: usuario
  },
  {
    path: '/edit/:id?',
    name: 'usuario-edit',
    component: usuarioEdit
  }
];

// todas as rotas
const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
});

var vm = new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
